
# Products and Images CRUD API

REST API developed with the best of Spring Boot, JPA, Hibernate, H2 and Jersey.
The are two entities involved **Product** and **Image**.
To run this project, you need to have Java *8 and Maven installed.
The application will be available at: http://localhost:8080/

## Running
To run the application run the command:
```java
mvn clean spring-boot:run
```
Endpoints available:

* **GET** */api/product* - Retrieves a list of Products with its SubResources
* **GET** */api/product/{id}* - Retrieves a specific Product with its SubResources
* **GET** */api/product/summary* - Retrieves a list of Products with only names and descriptions
* **GET** */api/product/summary/{id}* - Retrieves a specific Product with only its name and description
* **POST** */api/product* - Creates a new Product. Example:
```http
POST /api/product HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

name=Product+2&description=Description+2&parentId=2
```
* **PUT** */api/product/{id}* - Updates a specific product. Example:
```http
PUT /api/product/2 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

name=nomeNovo&description=description&parentId=1
```
* **DELETE** */api/product/{id}* - Deletes a specific product

* **GET** */api/image* - Retrieves a list of Images
* **GET** */api/image/{id}* - Retrieves a specific Image
* **POST** */api/image* - Creates a new Image. Example:
```http
POST /api/image HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

type=.ttf&productId=1
```
* **PUT** */api/image/{id}* - Updates a specific image. Example:
```http
PUT /api/image/6 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

type=.ssh&productId=1
```
* **DELETE** */api/image/{id}* - Deletes a specific image

Default Response Data Example:
```json
{
    "responseCode": "OK",
    "message": "",
    //data to be returned. Changes according to endpoint output class
    "data": [
        {
            "name": "Product 1",
            "description": "Description 1"
        },
        {
            "name": "Waaaat",
            "description": "desc"
        }
    ]
}
```

## Improvements
Nor the Integration tests or the API tests were finished in time, so they were not delivered. There was not created Endpoints to retrieve only the images or the products per product as well, due to time issues. However, these information can be easily retrieved by the main API which returns all info.

