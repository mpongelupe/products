
--CREATE TABLE product (
--    id             int      NOT NULL AUTO_INCREMENT,
--    parent_product_id int,
--    name      varchar(255) NOT NULL,
--    description varchar(600) NOT NULL,
--    PRIMARY KEY (id),
--    FOREIGN KEY (parent_product_id) REFERENCES product (id)
--) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
--
--CREATE TABLE image (
--    id             int      NOT NULL AUTO_INCREMENT,
--    product_id        int,
--    type          varchar(255) NOT NULL,
--    PRIMARY KEY (id),
--    FOREIGN KEY (product_id) REFERENCES product (id)
--) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
--
INSERT INTO product (name, description) VALUES ('Product 1', 'Description 1');
INSERT INTO product (name, description, parent_product_id) VALUES ('Waaaat', 'desc', 1);
INSERT INTO image (type, product_id) VALUES ('png', 1);
INSERT INTO image (type, product_id) VALUES ('jpg', 1);
INSERT INTO image (type, product_id) VALUES ('gif', 2);
