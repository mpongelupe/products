package com.mateusp.products;

public class Constants {

    public static final String API_PACKAGE_NAME = "com.mateusp.products.api";
    public static final String API_URL_MAPPING = "/api/*";
    public static final String MODEL_PACKAGE_NAME = "com.mateusp.products.model";
    public static final String PRODUCTS_API_PATH = "/product/";
    public static final String IMAGE_API_PATH = "/image/";
}
