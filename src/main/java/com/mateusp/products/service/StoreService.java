package com.mateusp.products.service;


import com.mateusp.products.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class StoreService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ImageRepository imageRepository;

    public Product getProduct(Long id) {
        return productRepository.findOne(id);
    }


    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }



    public Product saveProduct(String name, String description, String parentProductIdStr) {
        Product product = null;
        if(parentProductIdStr != null) {
            Long parentProduct = Long.parseLong(parentProductIdStr);
            product = new Product(name, description, parentProduct);
        } else {
            product = new Product(name, description);
        }
        productRepository.save(product);
        return product;
    }


    public Product updateProduct(Long id, String name, String description, String parentProductIdStr) {
        Product product = null;
        if(parentProductIdStr != null) {
            Long parentProduct = Long.parseLong(parentProductIdStr);
            product = new Product(name, description, parentProduct);
        } else {
            product = new Product(name, description);
        }
        product.setId(id);
        productRepository.save(product);
        return product;
    }

    public Product deleteProduct(Long id) {
        Product product = getProduct(id);
        productRepository.delete(id);
        return product;
    }

    public List<Image> getAllImages() {
        List<Image> images = new ArrayList<>();
        imageRepository.findAll().forEach(images::add);
        return images;
    }

    public Image saveImage(String type, String productId) {
        Image image = null;
        if(productId != null) {
            Long parentProduct = Long.parseLong(productId);
            image = new Image(type, parentProduct);
        } else {
            image = new Image(type);
        }
        imageRepository.save(image);
        return image;
    }

    public Image getImage(Long id) {
        return imageRepository.findOne(id);
    }

    public Image updateImage(Long id, String type, String parentProductIdStr) {
        Image image = null;
        if(parentProductIdStr != null) {
            Long parentProduct = Long.parseLong(parentProductIdStr);
            image = new Image(type, parentProduct);
        } else {
            image = new Image(type);
        }
        image.setId(id);
        imageRepository.save(image);
        return image;
    }

    public Image deleteImage(Long id) {
        Image image = getImage(id);
        imageRepository.delete(id);
        return image;
    }

    public Collection<ProductSummary> listProductSummaries() {
        return productRepository.findSummaryForAll();
    }

    public ProductSummary getProductSummaryById(Long id) {
        return productRepository.findSummaryById(id);
    }
}
