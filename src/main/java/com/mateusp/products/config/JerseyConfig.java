package com.mateusp.products.config;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.filter.AbstractRequestLoggingFilter;
import org.springframework.web.filter.RequestContextFilter;

import static com.mateusp.products.Constants.API_PACKAGE_NAME;

public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(RequestContextFilter.class);
        packages(API_PACKAGE_NAME);
        register(LoggingFilter.class);
    }


}
