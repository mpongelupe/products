package com.mateusp.products.api;

public class AbstractResponse<T> {

    private static final String R_MSG_EMPTY = "";
    private static final String R_CODE_OK = "OK";

    private final String responseCode;
    private final String message;

    private T data;

    /**
     * A Creates a new instance of AbstractResponse
     *
     * @param code
     * @param message
     */
    public AbstractResponse(final String code, final String message) {
        this.message = message == null ? AbstractResponse.R_MSG_EMPTY : message;
        this.responseCode = code == null ? AbstractResponse.R_CODE_OK : code;
    }

    /**
     * @return the message
     */
    public String getMessage() {

        return this.message;
    }

    /**
     * @return the data
     */
    public T getData() {

        return this.data;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {

        return this.responseCode;
    }

    /**
     * sets the data object
     *
     * @param obj
     * @return
     */
    public void setData(final T obj) {

        this.data = obj;
    }
}

