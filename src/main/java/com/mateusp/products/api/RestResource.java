package com.mateusp.products.api;

import com.mateusp.products.model.Image;
import com.mateusp.products.model.Product;
import com.mateusp.products.model.ProductSummary;
import com.mateusp.products.service.StoreService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Collection;
import java.util.List;

import static com.mateusp.products.Constants.IMAGE_API_PATH;
import static com.mateusp.products.Constants.PRODUCTS_API_PATH;


@Path("/")
@Component
public class RestResource {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RestResource.class);

    @Autowired
    private StoreService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH)
    public AbstractResponse listProducts() {
        try {
            AbstractResponse<List<Product>> abstractResponse = new AbstractResponse(null, null);
            List<Product> products = service.getAllProducts();
            abstractResponse.setData(products);
            return abstractResponse;
        } catch(Exception e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH+"summary")
    public AbstractResponse listProductSummary() {
        try {
            AbstractResponse<Collection<ProductSummary>> abstractResponse = new AbstractResponse(null, null);
            Collection<ProductSummary> products = service.listProductSummaries();
            abstractResponse.setData(products);
            return abstractResponse;
        } catch(Exception e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH+"summary/{itemId}")
    public AbstractResponse getProductSummary(@PathParam("itemId") Long id) {
        try {
            if(id != null) {
                AbstractResponse<ProductSummary> abstractResponse = new AbstractResponse(null, null);
                ProductSummary product = service.getProductSummaryById(id);
                abstractResponse.setData(product);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH)
    public AbstractResponse addProduct(@FormParam("name") String name,
                                       @FormParam("description") String description,
                                       @FormParam("parentId") String parentProductIdStr) {
        try {

            if(name != null && description != null){
                AbstractResponse<Product> abstractResponse = new AbstractResponse(null, "Saved...");
                Product product = service.saveProduct(name, description, parentProductIdStr);
                abstractResponse.setData(product);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH + "{itemId}")
    public AbstractResponse getProduct(@PathParam("itemId") Long id) {
        try {
            if(id != null){
                AbstractResponse<Product> abstractResponse = new AbstractResponse(null, null);
                Product product = service.getProduct(id);
                abstractResponse.setData(product);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH + "{itemId}")
    public AbstractResponse updateProduct(@PathParam("itemId") Long id,
                                          @FormParam("name") String name,
                                          @FormParam("description") String description,
                                          @FormParam("parentId") String parentProductIdStr) {
        try {
            if(id != null){
                AbstractResponse<Product> abstractResponse = new AbstractResponse(null, "Updated product with id: " + id);
                Product product = service.updateProduct(id, name, description, parentProductIdStr);
                abstractResponse.setData(product);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path(PRODUCTS_API_PATH + "{itemId}")
    public AbstractResponse deleteProduct(@PathParam("itemId") Long id) {
        try {
            if(id != null){
                AbstractResponse<Product> abstractResponse = new AbstractResponse(null, "Deleted Product: " + id);
                Product product = service.deleteProduct(id);
                abstractResponse.setData(product);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(IMAGE_API_PATH)
    public AbstractResponse listImages() {
        try {
            AbstractResponse<List<Image>> abstractResponse = new AbstractResponse(null, null);
            List<Image> images = service.getAllImages();
            abstractResponse.setData(images);
            return abstractResponse;
        } catch(Exception e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path(IMAGE_API_PATH)
    public AbstractResponse addImage(@FormParam("type") String type,
                                     @FormParam("productId") String productId) {
        try {

            if(type != null){
                AbstractResponse<Image> abstractResponse = new AbstractResponse(null, "Saved new image...");
                Image image = service.saveImage(type, productId);
                abstractResponse.setData(image);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path(IMAGE_API_PATH + "{itemId}")
    public AbstractResponse getImage(@PathParam("itemId") Long id) {
        try {
            if(id != null){
                AbstractResponse<Image> abstractResponse = new AbstractResponse(null, null);
                Image image = service.getImage(id);
                abstractResponse.setData(image);
                return abstractResponse;
            }
            else {
                logger.info("Invalid parameter " + id.toString());
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path(IMAGE_API_PATH + "{itemId}")
    public AbstractResponse updateImage(@PathParam("itemId") Long id,
                                        @FormParam("type") String type,
                                        @FormParam("parent_product_id") String parentProductIdStr) {
        try {
            if(id != null){
                AbstractResponse<Image> abstractResponse = new AbstractResponse(null, "Updated Image entity: "+id);
                Image image = service.updateImage(id, type, parentProductIdStr);
                abstractResponse.setData(image);
                return abstractResponse;
            }
            else {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path(IMAGE_API_PATH + "{itemId}")
    public AbstractResponse deleteImage(@PathParam("itemId") Long id) {
        try {
            if(id != null){
                AbstractResponse<Image> abstractResponse = new AbstractResponse(null, "Deleted Image entity: "+id);
                Image image = service.deleteImage(id);
                abstractResponse.setData(image);
                return abstractResponse;
            }
            else {
                logger.info("Invalid parameter " + id.toString());
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
        } catch(Exception e) {
            logger.error("Error processing request ...", e);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

}
