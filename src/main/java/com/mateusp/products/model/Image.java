package com.mateusp.products.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;


@Entity
@Table(name = "image")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="product_id")
    private Product product;

    private String type;

    public Image() {

    }

    public Image(String type) {
        super();
        this.type = type;
    }

    public Image(String type, Product product) {
        super();
        this.type = type;
        this.product = product;
    }

    public Image(String type, Long parentProduct) {
        this.type = type;
        this.product = new Product();
        this.product.setId(parentProduct);
    }

    @Override
    public String toString() {
        return String.format(
                "Image[id=%d, type='%s', productId='%s']",
                id, type, product.getId());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}