package com.mateusp.products.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface ProductRepository extends CrudRepository<Product, Long>{

    @Query("SELECT new com.mateusp.products.model.ProductSummary(p.name, p.description) FROM Product p")
    Collection<ProductSummary> findSummaryForAll();

    @Query("SELECT new com.mateusp.products.model.ProductSummary(p.name, p.description) FROM Product p WHERE p.id = :id")
    ProductSummary findSummaryById(@Param("id") Long id);


}
