package com.mateusp.products.model;

public class ProductSummary {
    private String name, description;

    public ProductSummary(String name, String description) {
        this.name = name;
        this.description = description;
    }
    public String getName(){
        return this.name;
    }
    public String getDescription(){
        return this.description;
    }
}
