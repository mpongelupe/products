package com.mateusp.products.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonBackReference
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="parent_product_id")
    private Product parent;

    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "product", targetEntity = Image.class)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Image> images;

    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "parent", targetEntity = Product.class)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Product> children;

    private String name;

    private String description;

    public Product() {

    }

    public Product(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public Product(Long id, String name, String description) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Product(String name, String description, Long parentProductId) {
        this.name = name;
        this.description = description;
        this.parent = new Product();
        this.parent.setId(parentProductId);
    }

    public Product(Long id, Product parent, Collection<Image> images, Collection<Product> children, String name, String description) {

        this.id = id;
        this.parent = parent;
        this.images = images;
        this.children = children;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, name='%s', description='%s']",
                id, name, description);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getParent() {
        return parent;
    }

    public void setParent(Product parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Image> getImages() {
        return images;
    }

    public void setImages(Collection<Image> images) {
        this.images = images;
    }

    public Collection<Product> getChildren() {
        return children;
    }

    public void setChildren(Collection<Product> children) {
        this.children = children;
    }
}

