package com.mateusp.products;

import com.mateusp.products.api.RestResource;
import com.mateusp.products.config.JerseyConfig;
import com.mateusp.products.model.Image;
import com.mateusp.products.service.StoreService;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


import static com.mateusp.products.Constants.API_URL_MAPPING;
import static com.mateusp.products.Constants.MODEL_PACKAGE_NAME;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {RestResource.class, StoreService.class})
@EntityScan(MODEL_PACKAGE_NAME)
@SpringBootApplication
public class ProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean jerseyServlet() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new ServletContainer(), API_URL_MAPPING);
		registration.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, JerseyConfig.class.getName());
		return registration;
	}
}
